import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);

        System.out.println("Please entry host : ");
        String host=sc.next();
        System.out.println("Please entry port : ");
        int port=sc.nextInt();
        InetAddress add=null;
        try {
            add=InetAddress.getByName(host);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        System.out.println("\nPlease entry file name ");
        String fileName=sc.next();
        Client client=null;
        try {
             client=new Client(port,add,fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Thread clientThread=new Thread(client);
        Server server=new Server(add,port);
        Thread serverThread=new Thread(server);
        serverThread.start();
        clientThread.start();
    }

}