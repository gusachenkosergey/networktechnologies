import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class Server  implements Runnable {
    private final int port;
    private final InetAddress host;
    public Server(InetAddress host, int port) {
        this.port = port;
        this.host = host;
    }

    public synchronized void startServer() throws IOException {
        ServerSocket serverSocket = new ServerSocket(port);
        while (true) {
            try {
                serverSocket.setSoTimeout(5000);
               Socket socket = serverSocket.accept();
                System.out.println("A new client is connected : " + socket);
                BufferedInputStream in = new BufferedInputStream(socket.getInputStream());
                BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream("outfile"));
                ClientHandler clientHandler = new ClientHandler(socket, out, in);
                Thread thread = new Thread(clientHandler);
                thread.start();
            } catch (SocketTimeoutException d){
                System.out.println("\n Cannot connection to this port.Connection is unreachable");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void run() {
        try {
            startServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
