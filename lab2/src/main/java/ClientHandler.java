import java.io.*;
import java.net.Socket;
import java.util.Timer;

public class ClientHandler implements Runnable {

    private Socket socket;
    private BufferedInputStream in;
    private BufferedOutputStream out;
    private byte[] buffer;
    private TimerHandler timeHandler;
    private Timer timer;
    private int byteCounter;
    private long startTime, stopTime, timeSupport;

    public ClientHandler(Socket socket, BufferedOutputStream out, BufferedInputStream in) {
        this.socket = socket;
        this.in = in;
        this.out = out;
    }

    @Override
    public void run() {
        try {
            readFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private synchronized void readFile() throws IOException {
        timeHandler = new TimerHandler(this);
        timer = new Timer(true);
        timer.scheduleAtFixedRate(timeHandler, 0, 3000);
        int sizeOfFile = in.read();
        buffer = new byte[sizeOfFile];
        int length = 0;
        timeSupport = System.nanoTime();
        while ((length = in.read(buffer)) > 0) {
            startTime = System.nanoTime();
            out.write(buffer, 0, length);
            startTime = System.nanoTime() - startTime;
            stopTime = System.nanoTime() - timeSupport;
            byteCounter++;
            System.out.print("#");
        }
        in.close();
        out.flush();
        out.close();
        socket.close();
        System.out.println("\nDone!");
        System.out.println("\nSend was successfully !");
    }

    public long getStopTime() {
        return stopTime;
    }

    public long getStartTime() {
        return startTime;
    }

    public int getByteCounter() {
        return byteCounter;
    }
}
