import java.util.TimerTask;

public class TimerHandler extends TimerTask {
    private ClientHandler clientHandler;

    public TimerHandler(ClientHandler clientHandler) {
        this.clientHandler=clientHandler;
    }

    @Override
    public void run() {
        printAverageSpeed();
        printMomentumSpeed();
    }

    private void printMomentumSpeed() {
        System.out.println();
        if(clientHandler.getStartTime()!=0) {
            System.out.println("\nMomentum speed is byte/nanoseconds = " +  ((double)1/clientHandler.getStartTime()));
        }
    }

    private void printAverageSpeed() {
        if(clientHandler.getStopTime()!=0) {
            System.out.printf("\nAverage speed is bytes/nanoseconds = "+ ((double)clientHandler.getByteCounter() / clientHandler.getStopTime()));
        }
    }
}
