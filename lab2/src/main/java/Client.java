import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
public class Client implements Runnable {
    private final int port;
    private final String fileName;
    private InetAddress host;
    private File file;
    private byte[] buffer;
    public Client(int port, InetAddress host, String fileName) throws IOException  {
        this.port = port;
        this.fileName = fileName;
        this.host=host;
        file=new File(fileName);
        buffer=new byte[Math.toIntExact(file.length())];
    }
    public synchronized void sendFile() throws IOException {
        System.out.println("Connecting to ... " + host + ":" + port + "...");
        Socket client = new Socket("localhost", port);
        BufferedInputStream in = new BufferedInputStream(new FileInputStream(fileName));
        BufferedOutputStream out = new BufferedOutputStream(client.getOutputStream());
        out.write(Math.toIntExact(file.length()));
        System.out.println("\nThe size of file is send = "+file.length());
        int len = 0;
        while ((len =  in.read(buffer)) > 0) {
            out.write(buffer, 0, len);
            System.out.print("#");
        }
        in.close();
        out.flush();
        out.close();
        client.close();
        System.out.println("\nDone!");
    }

    @Override
    public void run() {
        try {
            sendFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}