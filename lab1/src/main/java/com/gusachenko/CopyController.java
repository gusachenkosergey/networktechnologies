package com.gusachenko;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class CopyController {
    public final ConcurrentHashMap<UUID, Integer> copies = new ConcurrentHashMap<UUID, Integer>();
    private final int CHECK_VALUE = 2;
    private final static Object mutex = new Object();
    private AtomicInteger counter = new AtomicInteger();
    private boolean flag=false;
    public CopyController() {
    }

    public synchronized void add(String id) {

        if (!copies.containsKey(UUID.fromString(id))) {
            copies.put(UUID.fromString(id), CHECK_VALUE);
            counter.incrementAndGet();
        }

    }

    public synchronized void update(String id) {
        UUID uuid = UUID.fromString(id);
        for (Map.Entry<UUID, Integer> entry : copies.entrySet()) {
            int check = entry.getValue();
            check++;
            entry.setValue(check);
            if (entry.getValue() > CHECK_VALUE + 4) {
                copies.remove(entry.getKey());
            }
                System.out.println("Count of real work copy = " + copies.size());
            if (entry.getKey().equals(uuid)) {
                entry.setValue(CHECK_VALUE);
            }
        }
    }


}
