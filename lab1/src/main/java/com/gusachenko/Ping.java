package com.gusachenko;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.MulticastSocket;

public class Ping {
    public MulticastSocket socket = null;

    public Ping(int port) {
        try {
            this.socket = new MulticastSocket(port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Inet6Address getIPv6Addresses(InetAddress[] addresses) {
        for (InetAddress addr : addresses) {
            if (addr instanceof Inet6Address) {
                return (Inet6Address) addr;
            }
        }
        return null;
    }

    public boolean kindOfIp(InetAddress[] addresses) throws Exception {
        for (InetAddress addr : addresses) {
            if (addr instanceof Inet6Address) {
                getIp6(addr);
                return true;
            } else if (addr instanceof Inet4Address) {
                getIp4(addr);
                return false;
            } else {
                throw new Exception("Incorrect ip");
            }
        }
        return false;
    }

    private Inet4Address getIp4(InetAddress addr) {
        return (Inet4Address) addr;
    }

    private Inet6Address getIp6(InetAddress addr) {
        return (Inet6Address) addr;
    }

    public Inet4Address getIPv4Addresses(InetAddress[] addresses) {
        for (InetAddress addr : addresses) {
            if (addr instanceof Inet4Address) {
                return (Inet4Address) addr;
            }
        }
        return null;
    }

}