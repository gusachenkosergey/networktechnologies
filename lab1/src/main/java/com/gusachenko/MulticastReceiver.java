package com.gusachenko;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.logging.Logger;

public class MulticastReceiver implements Runnable {
    private static Logger log = Logger.getLogger(MulticastReceiver.class.getName());
    private MulticastSocket socket;
    private int port;
    private byte[] buf = new byte[256];
    private byte[] buffer = new byte[256];
    private CopyController copyController;
    private InetAddress group;
    private final static Object mutex = new Object();

    public MulticastReceiver(InetAddress inet, int port,MulticastSocket socket) {
        this.socket=socket;
        copyController = new CopyController();
        group = inet;
        this.port = port;

    }

    @Override
    public void run() {
        try {
            socket.joinGroup(group);
        } catch (IOException e) {
            System.out.println("join is failed");
            e.printStackTrace();
        }
        while (!Thread.currentThread().isInterrupted()) {
            DatagramPacket packet = new DatagramPacket(buf, buf.length);
            try {
                synchronized (mutex) {
                    socket.receive(packet);
                    buffer = packet.getData();
                    String id = new String(buffer, 0, packet.getLength());
                    copyController.update(id);
                    copyController.add(id);
                    log.info("The message is receive -" + id);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            socket.leaveGroup(group);
        } catch (IOException e) {
            e.printStackTrace();
        }
        socket.close();
    }
}