package com.gusachenko;

import java.io.IOException;
import java.net.*;
import java.util.UUID;
import java.util.logging.Logger;

public class MulticastPublisher implements Runnable {
    private static Logger log = Logger.getLogger(MulticastPublisher.class.getName());

    private MulticastSocket socket;
    private InetAddress group;
    private byte[] buf;
    private int port;
    private UUID id;

    public MulticastPublisher(int port, InetAddress inet, MulticastSocket socket) {
        id=UUID.randomUUID();
        this.socket=socket;
        this.port = port;
        this.group = inet;
    }

    public void multicast(String multicastMessage) throws IOException {

        buf = multicastMessage.getBytes();
        DatagramPacket packet = new DatagramPacket(buf, buf.length, group, port);
        socket.send(packet);
        socket.close();
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                multicast(id.toString());
                log.info("The message is send = "+id);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
