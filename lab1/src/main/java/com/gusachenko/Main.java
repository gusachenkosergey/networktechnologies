package com.gusachenko;

import java.io.IOException;
import java.net.*;
import java.util.Scanner;
import java.util.UUID;

public class Main {

    public static  void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please entry host ");
        String host = scanner.next();
        System.out.println("Please entry port");
        int port = scanner.nextInt();
        Ping ping = new Ping(port);

        try {
            InetAddress[] inet = InetAddress.getAllByName(host);
            boolean status = ping.kindOfIp(inet);
            if(status) {
                MulticastSocket socket = new MulticastSocket(port);
                Inet6Address add=ping.getIPv6Addresses(inet);
                MulticastPublisher multicastPublisher=new MulticastPublisher(port,add,socket);
                MulticastReceiver multicastReceiver=new MulticastReceiver(add,port,socket);
                Thread receiverThread=new Thread(multicastReceiver);
                Thread publisherThread=new Thread(multicastPublisher);
                receiverThread.start();
                publisherThread.start();
            }else{
                MulticastSocket socket = new MulticastSocket(port);
                Inet4Address add=ping.getIPv4Addresses(inet);
                MulticastPublisher multicastPublisher=new MulticastPublisher(port,add,socket);
                MulticastReceiver multicastReceiver=new MulticastReceiver(add,port,socket);
                Thread receiverThread=new Thread(multicastReceiver);
                Thread publisherThread=new Thread(multicastPublisher);
                receiverThread.start();
                publisherThread.start();
            }

        } catch (Exception e ) {
            e.printStackTrace();
        }

    }


}
